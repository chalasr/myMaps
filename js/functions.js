var map;
var panel;
var service;
var initialize;
var infoWindow;
var calculate;
var direction;
var previousPosition = null;

$(function(){
    $('#drivicon').click(function(){
      $('#drivicon').css('border', '2px dotted #006dcc');
      $('#walkicon').css('border', 'none');
      $('#cycleicon').css('border', 'none');
    });
    $('#walkicon').click(function(){
      $('#walkicon').css('border', '2px dotted #006dcc');
      $('#drivicon').css('border', 'none');
      $('#cycleicon').css('border', 'none');
    });
    $('#cycleicon').click(function(){
      $('#cycleicon').css('border', '2px dotted #006dcc');
      $('#drivicon').css('border', 'none');
      $('#walkicon').css('border', 'none');
    });
});

function successCallback(position){
  map.panTo(new google.maps.LatLng(position.coords.latitude, position.coords.longitude));
  var marker = new google.maps.Marker({
    position: new google.maps.LatLng(position.coords.latitude, position.coords.longitude),
    map: map
  });
  if (previousPosition){
    var newLineCoordinates = [
       new google.maps.LatLng(previousPosition.coords.latitude, previousPosition.coords.longitude),
       new google.maps.LatLng(position.coords.latitude, position.coords.longitude)
    ];

    var newLine = new google.maps.Polyline({
      path: newLineCoordinates,
      strokeColor: "#FF0000",
      strokeOpacity: 1.0,
      strokeWeight: 2
    });
    newLine.setMap(map);
  }
  previousPosition = position;
};

initialize = function(){
  var geocoder = new google.maps.Geocoder();
  var latLng = new google.maps.LatLng(48.858565, 2.347198); // Correspond au coordonnées de Lille
  var myOptions = {
    zoom      : 14, // Zoom par défaut
    center    : latLng, // Coordonnées de départ de la carte de type latLng
    mapTypeId : google.maps.MapTypeId.ROADMAP, // Type de carte, différentes valeurs possible HYBRID, ROADMAP, SATELLITE, TERRAIN
    maxZoom   : 20,
    styles: [
      {
        stylers: [
          { visibility: 'simplified' }
        ]
      },
      {
        elementType: 'labels',
        stylers: [
          { visibility: 'on' }
        ]
      }
    ]
  };

  if (navigator.geolocation){
    var watchId = navigator.geolocation.watchPosition(successCallback, null, {enableHighAccuracy:true});
      navigator.geolocation.getCurrentPosition(function(position) {
        pos = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
        //document.getElementById('origin').value = pos;
        map.setCenter(pos);
        geocoder.geocode({'latLng': pos}, function(results, status) {
          if (status == google.maps.GeocoderStatus.OK) {
            if (results[0]) {
              $('#locadest').click(function(){
                $('#destination').val(results[0].formatted_address);
              });
              $('#locaorg').click(function(){
                $('#origin').val(results[0].formatted_address);
              });
            }
          } else {
            $('#origin').val("erreur");
          }
        });
      }, function() {
        handleNoGeolocation(true);
      });
  }else{
    alert("Votre navigateur ne prend pas en compte la géolocalisation HTML5");
  }

  map = new google.maps.Map(document.getElementById('map'), myOptions);
  panel = document.getElementById('panel');

  infoWindow = new google.maps.InfoWindow();
  service = new google.maps.places.PlacesService(map);

  var marker = new google.maps.Marker({
    position : latLng,
    map      : map,
    title    : "Lille"
    //icon     : "marker_lille.gif" // Chemin de l'image du marqueur pour surcharger celui par défaut
  });

  var contentMarker = [
      '<div id="containerTabs">',
        '<div id="tabs">',
          '<ul>',
            '<li><a href="#tab-1"><span>Lorem</span></a></li>',
            '<li><a href="#tab-2"><span>Ipsum</span></a></li>',
            '<li><a href="#tab-3"><span>Dolor</span></a></li>',
          '</ul>',
          '<div id="tab-1">',
            '<h3>Lille</h3><p>Suspendisse quis magna dapibus orci porta varius sed sit amet purus. Ut eu justo dictum elit malesuada facilisis. Proin ipsum ligula, feugiat sed faucibus a, <a href="http://www.google.fr">google</a> sit amet mauris. In sit amet nisi mauris. Aliquam vestibulum quam et ligula pretium suscipit ullamcorper metus accumsan.</p>',
          '</div>',
          '<div id="tab-2">',
           '<h3>Aliquam vestibulum</h3><p>Aliquam vestibulum quam et ligula pretium suscipit ullamcorper metus accumsan.</p>',
          '</div>',
          '<div id="tab-3">',
            '<h3>Pretium suscipit</h3><ul><li>Lorem</li><li>Ipsum</li><li>Dolor</li><li>Amectus</li></ul>',
          '</div>',
        '</div>',
      '</div>'
  ].join('');
/*
  var infoWindow = new google.maps.InfoWindow({
    content  : contentMarker,
    position : latLng
  });
*/
  google.maps.event.addListener(marker, 'click', function() {
    infoWindow.open(map,marker);
  });

  google.maps.event.addListener(infoWindow, 'domready', function(){ // infoWindow est biensûr notre info-bulle
    jQuery("#tabs").tabs();
  });

  direction = new google.maps.DirectionsRenderer({
    map   : map,
    panel : panel,
    draggable : true
  });

   google.maps.event.addListenerOnce(map, 'bounds_changed', performSearch);

};

function performSearch() {
  var request = {
    bounds: map.getBounds(),
    keyword: 'all'
  };
  service.radarSearch(request, callback);
}

function callback(results, status) {
  if (status != google.maps.places.PlacesServiceStatus.OK) {
    alert(status);
    return;
  }
  for (var i = 0, result; result = results[i]; i++) {
    createMarker(result);
  }
}

function createMarker(place) {
  var marker = new google.maps.Marker({
    map: map,
    position: place.geometry.location,
    icon: {
      // Star
      path: 'M 0,-24 6,-7 24,-7 10,4 15,21 0,11 -15,21 -10,4 -24,-7 -6,-7 z',
      fillColor: '#ffff00',
      fillOpacity: 1,
      scale: 1/4,
      strokeColor: '#bd8d2c',
      strokeWeight: 1
    }
  });

  google.maps.event.addListener(marker, 'click', function() {
    service.getDetails(place, function(result, status) {
      if (status != google.maps.places.PlacesServiceStatus.OK) {
        alert(status);
        return;
      }
      infoWindow.setContent(result.name);
      infoWindow.open(map, marker);
    });
  });
}
calculate = function(){
    travelinput = $('input[name="travelMode"]:checked').val();
    origin      = document.getElementById('origin').value; // Le point départ
    destination = document.getElementById('destination').value; // Le point d'arrivé
    if(origin && destination){
        var request = {
            origin      : origin,
            destination : destination,
            travelMode  : google.maps.DirectionsTravelMode[travelinput] // Mode de conduite
        }
        var directionsService = new google.maps.DirectionsService(); // Service de calcul d'itinéraire
        directionsService.route(request, function(response, status){ // Envoie de la requête pour calculer le parcours
            if(status == google.maps.DirectionsStatus.OK){
                direction.setDirections(response); // Trace l'itinéraire sur la carte et les différentes étapes du parcours
            }
        });
    }
       google.maps.event.addListenerOnce(map, 'bounds_changed', performSearch);
};

    save ={};
    saveExist = false;
     function verifSave(){
        if(localStorage.mapper_origin && localStorage.mapper_destination && localStorage.mapper_travelMode){
            save.mapper_origin      = localStorage.mapper_origin;
            save.mapper_destination = localStorage.mapper_destination;
            save.mapper_travelMode  = localStorage.mapper_travelMode;
            saveExist               = true;
            calculate(localStorage.mapper_origin, localStorage.mapper_destination, localStorage.mapper_travelMode, 'restore');
        } else{
            saveExist = false;
        }
    }

  saveTrip = function(){
        if(typeof localStorage != 'undefined'){
            localStorage.setItem('mapper_origin', $('#origin').val());
            localStorage.setItem('mapper_destination', $('#destination').val());
            localStorage.setItem('mapper_travelMode', $('input[name="travelMode"]:checked').val());
            saveExist = true;
        }
    }

    clearSave = function(){
        saveExist = false;
        localStorage.removeItem('mapper_origin');
        localStorage.removeItem('mapper_destination');
        localStorage.removeItem('mapper_travelMode');
    }

initialize();
verifSave();