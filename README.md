
Google Maps app' - made with Javascript and Google Maps API
================

[![SensioLabsInsight](https://insight.sensiolabs.com/projects/170159da-6a65-4f3b-b9a7-79a7867b462d/small.png)](https://insight.sensiolabs.com/projects/170159da-6a65-4f3b-b9a7-79a7867b462d)

[Demo on maps.chalasdev.fr](http://maps.chalasdev.fr)

This application was created by Robin CHALAS - FullStack Web Developer -  http://www.chalasdev.fr/

Problems? Issues?
--------------

Write a message on chalasdev.fr or create an issue

Getting Started
---------------

- Clone this repository in your server directory

``` git clone https://github.com/chalasr/Javascript_My_Maps ```

- Start

``` open your browser & go to localhost/Javascript_My_Maps/ ```

Enjoy drawing !

Credits
-------

Author : [Robin Chalas](http://www.chalasdev.fr/)

License
-------

Copyright (c) 2014-2015 [Robin Chalas](http://www.chaladev.fr/) [![License](http://img.shields.io/:license-gpl3-blue.svg)](http://www.gnu.org/licenses/gpl-3.0.html)
